package com.unitbean.loginexample.Preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

public class PreferencesWorker {
    public static String NULL = "null";
    private SharedPreferences preferences;
    private String name;
    private Context context;
    public PreferencesWorker(Context context, String name) {
        this.context=context;
        preferences = context.getSharedPreferences(name,context.MODE_PRIVATE);
    }
    public void set(String name, String value) {
         Log.d(name,"save "+name + ": "+value);
        SharedPreferences.Editor ed = preferences.edit();
        ed.putString(name, value);
        ed.commit();
    }

    public String get(String name) {
        return preferences.getString(name,NULL);
    }
    public void remove(String name) {
        set(name,NULL);
    }
    public boolean contains(String name) {
        boolean d = (preferences.contains(name) && !get(name).equals(NULL));

        Log.d(name,"contains "+name + ": "+get(name)+ " ("+d+")");
        return d;
    }
}
