package com.unitbean.loginexample.Data;

import com.unitbean.loginexample.Activities.MainActivity;
import com.unitbean.loginexample.Models.LoginData;
import com.unitbean.loginexample.Models.ProjectData;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class RetrofitService {

    static public Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(MainActivity.BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    static public RetrofitWorker caller = retrofit.create(RetrofitWorker.class);

    public interface RetrofitWorker {

        @FormUrlEncoded
        @POST("api/v1/auth/login/")
        Call<LoginData> submitLogin(
                @Field("grant_type") String grant,
                @Field("username") String username,
                @Field("password") String password );

        @GET("api/v1/db/project/city/")
        Call<ProjectData> getProjects(
                @Query("city") String city,
                @Query("access_token") String token);

    }

}
