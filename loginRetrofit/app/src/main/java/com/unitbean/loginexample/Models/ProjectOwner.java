package com.unitbean.loginexample.Models;

/**
 * Created by root on 12.08.16.
 */
public class ProjectOwner {
    public String avatar;
    public String email;
    public String firstName;
    public String lastName;
    public String description;
    public String city;
    public String phone;
    public String updatedAt;
    public String createdAt;

}
