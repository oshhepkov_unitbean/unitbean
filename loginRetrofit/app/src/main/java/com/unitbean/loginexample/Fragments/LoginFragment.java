package com.unitbean.loginexample.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.unitbean.loginexample.Activities.MainActivity;
import com.unitbean.loginexample.Models.LoginData;
import com.unitbean.loginexample.Data.RetrofitService;
import com.unitbean.loginexample.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 12.08.16.
 */
public class LoginFragment extends Fragment {
    Button button_login;
    TextView text_status;
    EditText edit_login, edit_password;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        button_login = (Button) root.findViewById(R.id.button_login);
        edit_login = (EditText) root.findViewById(R.id.edit_login);
        edit_password = (EditText) root.findViewById(R.id.edit_password);
        text_status = (TextView) root.findViewById(R.id.text_status);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLogin(edit_login.getText().toString(),edit_password.getText().toString());
            }
        });
        // TODO: remove this. its for debug
        edit_login.setText("pocteg@gmail.com");
        edit_password.setText("a4d3590a9b45909e1c874dfe40dfcfeb");
        ///
        return root;

    }

    void postLogin(String login, String password) {
        Call<LoginData> call = RetrofitService.caller.submitLogin(MainActivity.GRANT_TYPE,login,password);
        call.enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                String result;

                if (response.body()!=null) {
                    result = "Wait..";
                    getMainActivity().prefs.set(MainActivity.TOKEN_NAME,response.body().getToken());
                    getMainActivity().startFragment(new RecyclerViewFragment(),true);
                }
                else
                    result =" Login is invalid";
                text_status.setText(result);
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                Log.d("APP",call.request().body().toString());
                t.printStackTrace();
                text_status.setText("Internal error");
            }
        });
    }

public MainActivity getMainActivity() {
    return (MainActivity)this.getActivity();
}
}
