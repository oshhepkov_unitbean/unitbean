package com.unitbean.loginexample.Models;

/**
 * Created by root on 12.08.16.
 */
public class LoginData {
    String id,avatar,email,firstName,lastName,description,city,phone,updatedAt,createdAt;
    AccessToken oauth;

    public String getToken() {
        if (oauth!=null)
            return oauth.access_token;
        else
            return "null";
    }
}
