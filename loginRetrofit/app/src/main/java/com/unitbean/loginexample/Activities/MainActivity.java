package com.unitbean.loginexample.Activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.unitbean.loginexample.Fragments.LoginFragment;
import com.unitbean.loginexample.Fragments.RecyclerViewFragment;
import com.unitbean.loginexample.Preferences.PreferencesWorker;
import com.unitbean.loginexample.R;

public class MainActivity extends AppCompatActivity {
    public static final String
        CITY = "Volgograd",
        GRANT_TYPE = "password",
        TOKEN_NAME = "token",
        BASEURL = "http://rotary.unitbean.ru/",
        PREFERENCES_NAME = "app_pref";
        
    private static final int CONTAINER_ID = R.id.container ;
    public Toolbar toolbar;
    public PreferencesWorker prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        prefs = new PreferencesWorker(this,PREFERENCES_NAME);
 
        if (!prefs.contains(TOKEN_NAME)) 
            startFragment(new LoginFragment(), false);
        else
            startFragment(new RecyclerViewFragment(), false);
    }
 
    /**
     * Базовый метод для запуска фрагмента
     *
     * @param fragment новый фрагмент
     *
     */
    public void startFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(CONTAINER_ID, fragment);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }
}