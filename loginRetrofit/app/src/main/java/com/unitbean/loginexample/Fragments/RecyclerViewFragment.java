package com.unitbean.loginexample.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.unitbean.loginexample.Activities.MainActivity;
import com.unitbean.loginexample.Models.ProjectData;
import com.unitbean.loginexample.Data.RetrofitService;
import com.unitbean.loginexample.R;
import com.unitbean.loginexample.RecyclerViewExtended.RecyclerViewExtendedAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 12.08.16.
 */
public class RecyclerViewFragment extends Fragment{

    RecyclerView recyclerView;
    RecyclerViewExtendedAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        recyclerView = (RecyclerView)root.findViewById(R.id.recycerview_list);

        adapter = new RecyclerViewExtendedAdapter(getMainActivity());
        LinearLayoutManager llm = new LinearLayoutManager(this.getActivity().getBaseContext());

        //set adapter and layout manager for recycler view
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(llm);

        //set toolbar menu.
        setHasOptionsMenu(true);

        // load token from shared preferences.
        String token = getMainActivity().prefs.get(MainActivity.TOKEN_NAME);
        getProjects(MainActivity.CITY,token);

        return root;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
            case R.id.toolbar_logout:
                getMainActivity().prefs.remove(MainActivity.TOKEN_NAME);
                getMainActivity().startFragment(new LoginFragment(),false);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }
    public void getProjects(String city, String token) {
        Call<ProjectData> call = RetrofitService.caller.getProjects(city,token);
        call.enqueue(new Callback<ProjectData>() {
            @Override
            public void onResponse(Call<ProjectData> call, Response<ProjectData> response) {

                adapter.set(response.body().rows);
            }
            @Override
            public void onFailure(Call<ProjectData> call, Throwable t) {
                t.printStackTrace();
            }

        });
    }

    /*
    * Возвращает главную активити
    *
    */
    public MainActivity getMainActivity() {
        return (MainActivity)this.getActivity();
    }

}
