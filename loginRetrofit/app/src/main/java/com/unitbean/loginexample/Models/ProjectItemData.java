package com.unitbean.loginexample.Models;

import java.util.ArrayList;

/**
 * Created by root on 12.08.16.
 */
public class ProjectItemData {
    public String id;
    public String title;
    public String description;
    public String phone;
    public String city;
    public String address;
    public String webUrl;
    public String email;
    public String longitude;
    public String latitude;
    public String isEditable;
    public ArrayList<String> pictures = new ArrayList<String>();
    public ArrayList<String> categories = new ArrayList<String>();
    public ProjectOwner owner;

}
