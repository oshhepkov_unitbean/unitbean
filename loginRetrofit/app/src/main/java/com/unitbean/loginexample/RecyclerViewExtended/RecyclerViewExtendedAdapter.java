package com.unitbean.loginexample.RecyclerViewExtended;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.unitbean.loginexample.Activities.MainActivity;
import com.unitbean.loginexample.Models.ProjectItemData;
import com.unitbean.loginexample.R;

import java.util.ArrayList;

/**
 * Created by root on 12.08.16.
 */

    public class RecyclerViewExtendedAdapter extends RecyclerView.Adapter<RecyclerViewExtendedAdapter.ViewHolder>{
    private ArrayList<ProjectItemData> items;
    Context context;
    public RecyclerViewExtendedAdapter(Context context) {

        super();
        this.context=context;
        items = new ArrayList<>();
    }
    public void add(ProjectItemData data) {
        items.add(data);
        notifyDataSetChanged();
    }
    public void set(ArrayList<ProjectItemData> data) {
        if (data!=null)
        items = data;
        notifyDataSetChanged();
    }
    @Override
    public RecyclerViewExtendedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_item, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewExtendedAdapter.ViewHolder holder, int position) {
        ProjectItemData project= items.get(position);
        holder.title.setText(project.title);
        holder.name.setText(project.owner.firstName+" "+project.owner.lastName);
         if (project.pictures.size()>-1) {
            holder.image.setVisibility(View.VISIBLE);
            Picasso.with(context)
            //.load(MainActivity.BASEURL+"pics/"+project.pictures.get(0)) //project.owner.avatar)
              .load(MainActivity.BASEURL+"pics/"+project.owner.avatar)
                .into(holder.image);
         } else {
             holder.image.setVisibility(View.GONE);
         }
    }

    @Override
    public int getItemCount() {
        if (items!=null)
            return items.size();
        else return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
            CardView cv;
            TextView title  ;
            TextView name;
            ImageView image;

        ViewHolder(View itemView) {
                super(itemView);
                cv = (CardView)itemView.findViewById(R.id.cardview_item);
                title = (TextView)itemView.findViewById(R.id.cardview_title);
                name = (TextView)itemView.findViewById(R.id.cardview_user);
                image = (ImageView)itemView.findViewById(R.id.cardview_image);
             }

        }

    }